/*
 * Copyright (c) 2013-2014 NVIDIA CORPORATION.  All rights reserved.
 *
 * NVIDIA Corporation and its licensors retain all intellectual property
 * and proprietary rights in and to this software and related documentation
 * and any modifications thereto.  Any use, reproduction, disclosure or
 * distribution of this software and related documentation without an express
 * license agreement from NVIDIA Corporation is strictly prohibited.
 */

#define LOG_TAG "TLK_Daemon"

#include <stdio.h>
#include <stdlib.h>
#include <string.h>
#include <errno.h>
#include <dirent.h>
#include <fcntl.h>
#include <sys/stat.h>
#include <sys/types.h>
#include <sys/ioctl.h>

#include <client/ote_client.h>
#include <common/ote_nv_uuid.h>
#include <common/ote_storage_protocol.h>
#include <service/ote_storage.h>

/* enable following to get ALOGV output */
/* #define LOG_NDEBUG	0 */

#include <android/log.h>
#include <utils/Log.h>

#define GETPOS(p)	(lseek(p->handle, 0, SEEK_CUR))

/* secure storage operation buffer */
static ote_ss_req_t	ss_req_buf __attribute__ ((aligned(0x1000)));

static char *get_request_name(int type)
{
	if (type == OTE_STORAGE_FILE_CREATE)
		return "CREATE";
	else if (type == OTE_STORAGE_FILE_DELETE)
		return "DELETE";
	else if (type == OTE_STORAGE_FILE_OPEN)
		return "OPEN";
	else if (type == OTE_STORAGE_FILE_CLOSE)
		return "CLOSE";
	else if (type == OTE_STORAGE_FILE_READ)
		return "READ";
	else if (type == OTE_STORAGE_FILE_WRITE)
		return "WRITE";
	else if (type == OTE_STORAGE_FILE_SEEK)
		return "SEEK";
	else if (type == OTE_STORAGE_FILE_GET_SIZE)
		return "GET_SIZE";
	else if (type == OTE_STORAGE_FILE_TRUNC)
		return "TRUNC";

	return "UNKNOWN";
}

static int check_dir(char *dirname)
{
	struct stat statbuf;
	int result;

	/*
	 * Check if specified directory exists. Android cleans the /data
	 * directory during file system encryption.
	 */
	result = stat(dirname, &statbuf);
	if (result == 0) {
		/* check access rights */
		if ((statbuf.st_mode & (S_IXUSR | S_IWUSR)) !=
			(S_IXUSR | S_IWUSR)) {
			return EACCES;
		}
	} else {
		/* another error */
		return errno;
	}

	return 0;
}

/*
 * Create specified directory if it doesn't currently exist.
 */
static int create_dir(char *dirname)
{
	int result;

	ALOGV("checking/creating %s\n", dirname);

	/* check if directory already exists */
	result = check_dir(dirname);
	if (result != 0) {
		if (result != ENOENT) {
			ALOGE("check_dir failed: %s\n",	dirname);
			return result;
		}

		/* read/write/search for owner only */
		result = mkdir(dirname, S_IXUSR | S_IWUSR | S_IRUSR);
		if (result != 0) {
			ALOGE("mkdir of %s failed: errno %d\n", dirname, errno);
			return errno;
		}
	}

	return 0;
}

/*
 * Delete specified directory if it is empty.
 */
static int delete_dir(char *dirname)
{
	DIR *dir;
	struct dirent *dirent;
	int numfiles = 0;
	int result;

	ALOGV("checking/deleting %s\n", dirname);

	dir = opendir(dirname);
	if (dir == NULL) {
		ALOGE("opendir of %s failed: errno %d\n", dirname, errno);
		return errno;
	}

	/* check to see if directory has more than just '.' and '..' */
	while ((dirent = readdir(dir)) != NULL) {
		if (++numfiles > 2)
			break;
	}

	closedir(dir);

	if (numfiles > 2)
		return 0;

	/* directory is empty so we can remove it */
	result = rmdir(dirname);
	if (result != 0) {
		ALOGE("rmdir of %s failed: errno %d\n", dirname, errno);
		return errno;
	}

	return 0;
}

static te_error_t convert_errno(int error)
{
	te_error_t te_err;

	switch (error) {
	case 0:
		te_err = OTE_SUCCESS;
		break;
	case EBADF:
	case EINVAL:
	case ENOTDIR:
		te_err = OTE_ERROR_BAD_PARAMETERS;
		break;
	case EBUSY:
		te_err = OTE_ERROR_BUSY;
	case ENXIO:
		te_err = OTE_ERROR_BAD_STATE;
		break;
	case ENOMEM:
		te_err = OTE_ERROR_OUT_OF_MEMORY;
		break;
	case ENOENT:
		te_err = OTE_ERROR_ITEM_NOT_FOUND;
		break;
	case EPERM:
		te_err = OTE_ERROR_ACCESS_DENIED;
		break;
	default:
		te_err = OTE_ERROR_GENERIC;
		break;
	}

	return te_err;
}

static te_error_t configure_ss_req_buffer(ote_ss_req_t *req_buf,
				te_session_t *session)
{
	te_operation_t op;
	te_service_id_t uuid = SERVICE_STORAGE_UUID;
	te_error_t result;

	te_init_operation(&op);

	/* open session */
	result = te_open_session(session, &uuid, &op);
	if (result != OTE_SUCCESS) {
		ALOGE("can't open session: status 0x%x\n", result);
		return result;
	}

	/* issue launch operation */
	te_operation_reset(&op);
	te_oper_set_command(&op, OTE_STORAGE_NS_CONFIG);
	te_oper_set_param_persist_mem_rw(&op, 0, req_buf, sizeof(*req_buf));
	result = te_launch_operation(session, &op);
	if (result != OTE_SUCCESS) {
		ALOGE("can't launch config operation: status 0x%x\n", result);
		goto exit;
	}

	ALOGI("ss buffer configured: addr %p size 0x%x\n",
		req_buf, (uint32_t)sizeof(*req_buf));

exit:
	te_operation_reset(&op);

	return result;
}

int main(int argc, char* argv[])
{
	ote_ss_req_t *req = &ss_req_buf;
	te_session_t session;
	int fd = 0;
	mode_t mode = S_IRUSR | S_IWUSR | S_IRGRP;
	char *dirname = NULL, *filename = NULL, *fullpath = NULL;
	char *storagedir = NULL;
	pid_t pid, sid;
	struct stat statbuf;
	int result = 0;
	char devicename[64];
	int dev_fd = 0;
	int ioctl_arg = 0;

	if (argc < 3) {
		ALOGE("insufficient input arguments\n");
		return 2;
	}

	while (argc != 0) {
		argv++;
		argc--;
		if (strncmp(argv[0], "--storagedir",
					strlen("--storagedir")) == 0) {
			storagedir = argv[1];
			argc -= 2;
		}
	}

	/* need a storagedir */
	if (storagedir == NULL) {
		ALOGE("no storagedir specified\n");
		return 1;
	}

	/* check for existence of specified storage dir */
	result = check_dir(storagedir);
	if (result != 0) {
		ALOGE("check_dir failed: dir=%s result=%d\n",
			storagedir, result);
		return 1;
	}

	/*
	 * Secure storage file are stored using paths of the form:
	 *
	 *   <storagedir>/tlk/<clientdir>/<clientfile>
	 *
	 * We use fullpath by updating the <clientdir> and <clientfile>
	 * components during each operation.
	 */
	fullpath = malloc(strlen(storagedir) + strlen("/tlk/") +
			OTE_MAX_DIR_NAME_LEN + OTE_MAX_FILE_NAME_LEN + 1);
	if (fullpath == NULL) {
		ALOGE("out of memory\n");
		return 1;
	}

	/* create '<storagedir>/tlk' if it doesn't exist */
	strncpy(fullpath, storagedir, strlen(storagedir));
	strncat(fullpath, "/tlk", strlen("/tlk"));
	if (create_dir(fullpath) != 0) {
		ALOGE("failed to create directory: %s\n", fullpath);
		return 1;
	}

	/* dirname will point to first spot in fullpath that's not static */
	strcat(fullpath, "/");
	dirname = fullpath + strlen(fullpath);

	/*
	 * Turns this application into a daemon => fork off parent process
	 */

	/* Fork off the parent process */
	pid = fork();
	if (pid < 0) {
		ALOGE("could not fork daemon\n");
		free(fullpath);
		return 1;
	}

	if (pid > 0) {
		/* parent */
		free(fullpath);
		return 0;
	}

	/* Change the file mode mask */
	umask(0077);

	/* Detach from the console */
	sid = setsid();
	if (sid < 0) {
		ALOGE("daemon group creation failed");
		free(fullpath);
		return 1;
	}

	ALOGI("started\n");

	strcpy(devicename, "/dev/");
	strcat(devicename, TLK_DEVICE_BASE_NAME);

	dev_fd = open(devicename, O_RDWR, 0);
	if (dev_fd < 0) {
		ALOGE("failed to open device: %s\n", devicename);
		free(fullpath);
		return 1;
	}

	/* configure secure storage buffer */
	if (configure_ss_req_buffer(req, &session) != OTE_SUCCESS) {
		ALOGE("can't configure storage buffer\n");
		free(fullpath);
		return 1;
	}

	for (;;) {

		/* get new file request */
		do {
			ioctl_arg = TE_IOCTL_SS_CMD_GET_NEW_REQ;
			result = ioctl(dev_fd, TE_IOCTL_SS_CMD, &ioctl_arg);
			if (result < 0)
				ALOGE("IOCTL_GET_NEW_REQ returned %d\n", errno);
		} while ((result < 0) && (errno == ENODATA));

		if (result < 0) {
			ALOGE("NEW_REQ failed: errno %d\n", errno);
			goto exit;
		}

		ALOGV("New request: %s\n", get_request_name(req->type));

		/*
		 * Service file system requests.
		 */
		req->result = OTE_SUCCESS;
		switch (req->type) {
		case OTE_STORAGE_FILE_CREATE:
		{
			ote_file_create_params_t *params;
			int flags;
			ssize_t cnt;

			params = &req->params.f_create;

			/* append dirname */
			strncpy(dirname, params->dname,
				strlen(params->dname)+1);

			/* check/create /data/tlk/<dirname> directory */
			result = create_dir(fullpath);
			if (result != 0) {
				req->result = convert_errno(errno);
				goto wait;
			}

			strcat(dirname, "/");

			/* append filename */
			filename = dirname + strlen(dirname);
			strncpy(filename, params->fname,
				strlen(params->fname)+1);

			/*
			 * All files are created with read/write permission.
			 * Also use the O_EXCL flag to ensure file doesn't
			 * already exist.
			 */
			flags = (O_CREAT|O_EXCL|O_RDWR);

			ALOGV(" filename %s\n", fullpath);

			fd = open(fullpath, flags, mode);
			if (fd < 0) {
				ALOGE("failed to create %s errno %d\n",
					fullpath, errno);
				req->result = convert_errno(errno);
				goto wait;
			}

			close(fd);
			break;
		}
		case OTE_STORAGE_FILE_DELETE:
		{
			ote_file_delete_params_t *params;

			params = &req->params.f_delete;

			/* append dirname */
			strncpy(dirname, params->dname,
				strlen(params->dname)+1);
			strcat(dirname, "/");

			/* append filename */
			filename = dirname + strlen(dirname);
			strncpy(filename, params->fname,
				strlen(params->fname)+1);

			ALOGV(" filename %s\n", fullpath);

			/* remove file */
			unlink(fullpath);

			/* check to see if dir should be removed as well */
			*filename = '\0';
			result = delete_dir(fullpath);
			if (result != 0) {
				req->result = convert_errno(errno);
				goto wait;
			}

			break;
		}
		case OTE_STORAGE_FILE_OPEN:
		{
			ote_file_open_params_t *params;
			int fd, flags;

			params = &req->params.f_open;

			/* append dirname */
			strncpy(dirname, params->dname,
				strlen(params->dname)+1);
			strcat(dirname, "/");

			/* append filename */
			filename = dirname + strlen(dirname);
			strncpy(filename, params->fname,
				strlen(params->fname)+1);

			/*
			 * All files are created with read/write permission.
			 * Also use the O_SYNC flag to ensure writes are
			 * sync'd to the storage device.
			 */
			flags = (O_RDWR|O_SYNC);

			ALOGV(" filename %s flags 0x%x mode 0x%x\n",
				params->fname, flags, mode);

			fd = open(fullpath, flags, mode);
			if (fd < 0) {
				ALOGE("failed to open %s\n", filename);
				req->result = convert_errno(errno);
				goto wait;
			}
			params->handle = fd;
			break;
		}
		case OTE_STORAGE_FILE_CLOSE:
		{
			ote_file_close_params_t *params;

			params = &req->params.f_close;

			ALOGV(" handle 0x%x\n", params->handle);

			if (close(params->handle) < 0) {
				ALOGE("failed to close handle %d\n",
					params->handle);
				req->result = convert_errno(errno);
			}

			break;
		}
		case OTE_STORAGE_FILE_READ:
		{
			ote_file_read_params_t *params;
			ssize_t cnt;

			params = &req->params.f_read;

			ALOGV(" handle 0x%x pos 0x%x bytes 0x%x\n",
				params->handle, (int)GETPOS(params),
				params->data_size);

			cnt = read(params->handle, params->data,
				params->data_size);
			if (cnt <= 0) {
				ALOGE("read err: handle 0x%x errno %d\n",
					params->handle, errno);
				req->result = convert_errno(errno);
				goto wait;
			}

			/* how many bytes did we actually read? */
			params->data_size = cnt;

			ALOGV(" handle 0x%x bytes read 0x%x\n",
				params->handle, params->data_size);

			break;
		}
		case OTE_STORAGE_FILE_WRITE:
		{
			ote_file_write_params_t *params;
			ssize_t cnt;

			params = &req->params.f_write;

			ALOGV(" handle 0x%x pos 0x%x bytes 0x%x\n",
				params->handle, (int)GETPOS(params),
				params->data_size);

			cnt = write(params->handle, params->data,
					params->data_size);
			if (cnt <= 0) {
				ALOGE(" write err: handle 0x%x errno %d\n",
					params->handle, errno);
				req->result = convert_errno(errno);
				goto wait;
			}

			ALOGV(" handle 0x%x bytes written 0x%x\n",
				params->handle, params->data_size);

			break;
		}
		case OTE_STORAGE_FILE_GET_SIZE:
		{
			ote_file_get_size_params_t *params;

			params = &req->params.f_getsize;

			result = fstat(params->handle, &statbuf);
			if (result == 0) {
				params->size = statbuf.st_size;
			} else {
				req->result = convert_errno(errno);
			}

			ALOGV(" handle 0x%x size 0x%x\n",
				params->handle, params->size);

			break;
		}
		case OTE_STORAGE_FILE_SEEK:
		{
			ote_file_seek_params_t *params;
			int offset;

			params = &req->params.f_seek;

			ALOGV(" handle 0x%x offset 0x%x\n",
				params->handle, params->offset);

			if (lseek(params->handle,
					params->offset, SEEK_SET) < 0) {
				ALOGE("lseek failed\n");
				req->result = convert_errno(errno);
				goto wait;
			}
			break;
		}
		case OTE_STORAGE_FILE_TRUNC:
		{
			ote_file_trunc_params_t *params;

			params = &req->params.f_trunc;

			ALOGV(" handle 0x%x length 0x%x\n",
				params->handle, params->length);

			if (ftruncate(params->handle, params->length) < 0) {
				ALOGE("ftruncate failed\n");
				req->result = convert_errno(errno);
				goto wait;
			}
			break;
		}
		default:
			ALOGE("unknown request type: 0x%x\n", req->type);
			goto exit;
		}

wait:
		/* indicate completion */
		ioctl_arg = TE_IOCTL_SS_CMD_REQ_COMPLETE;
		ioctl(dev_fd, TE_IOCTL_SS_CMD, &ioctl_arg);
	}

exit:
	te_close_session(&session);

	if (dev_fd)
		close(dev_fd);
	free(fullpath);

	ALOGE("exit\n");
	return 0;
}
