#
# Copyright (c) 2013-2014 NVIDIA CORPORATION.  All Rights Reserved.
#
# NVIDIA CORPORATION and its licensors retain all intellectual property
# and proprietary rights in and to this software, related documentation
# and any modifications thereto.  Any use, reproduction, disclosure or
# distribution of this software and related documentation without an express
# license agreement from NVIDIA CORPORATION is strictly prohibited.

#ifeq (tlk,$(SECURE_OS_BUILD))

LOCAL_PATH:= $(call my-dir)

include $(CLEAR_VARS)
LOCAL_C_INCLUDES:= 3rdparty/nvidia/ote-lib/include
LOCAL_SRC_FILES:= tlk_daemon.c
LOCAL_MODULE:= tlk_daemon
LOCAL_STATIC_LIBRARIES:= \
		libc \
		liblog \
		libtlk_common \
		libtlk_client

LOCAL_FORCE_STATIC_EXECUTABLE := true
LOCAL_NVIDIA_NO_COVERAGE := true
include $(BUILD_EXECUTABLE)

#endif # SECURE_OS_BUILD == tlk
